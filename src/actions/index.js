let idMessage = 0;

export const pushMessage = (text, socket, room) => (
    console.log(`room : ${room}`),
    socket.emit(room, text),
    {
        type: 'PUSH_MESSAGE',
        id: idMessage++,
        text
    }
)

export const getMessages = (text) => (
    {
        type: 'GET_MESSAGES',
        id: idMessage++,
        text
    }
)

export const getListRooms = (rooms) => (
    {
        type: 'LIST_ROOMS',
        rooms
    }
)

export const authenticate = (login, password) => (
    {
        type: 'AUTHENTICATE',
        login: login,
        password: password
    }
)

export const pushUserProfile = (profile) => (
    {
        type: 'PUSH_PROFILE',
        profile
    }
)

export const getUserProfile = () => (
    {
        type: 'GET_PROFILE',
    }
)

export const setAvatar = (uri, name, ext, pic) => (
    {
        type: 'SET_AVATAR',
        uri: uri,
        name: name,
        ext: ext,
        pic: pic
    }
)