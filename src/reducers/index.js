import { combineReducers } from 'redux'
import chat from './Chat'
import avatar from './Avatar'
import rooms from  './Rooms'

export default combineReducers ({
    chat,
    avatar,
    rooms
})