export default function avatar(state = {
    uri: '',
    name: '',
    ext: '',
    pic: ''
}, action){
    switch (action.type)
    {
        case 'SET_AVATAR':
            return ({
                'uri': action.uri,
                'name': action.name,
                'ext': action.ext,
                'pic': action.pic
            })
        default:
            return state
    }
}