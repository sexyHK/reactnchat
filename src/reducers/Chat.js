export default function chat(state = [], action){
    console.log(`action : ${JSON.stringify(action)}`)
    switch (action.type)
    {
        case 'PUSH_MESSAGE':
            return [
                ...state,
                {
                    id: action.id,
                    message: action.text
                }    
            ]
        case 'GET_MESSAGES':
            return [
                ...state,
                {
                    id: action.id,
                    message: action.text
                }
            ]
        case 'AUTHENTICATE':
            return [
                {
                    token: action.token
                }
            ]
        default:
            return state
    }
}