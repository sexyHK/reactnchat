export default function rooms(state = 
    {
        selected: '',
        room: [
            {
                id: '',
                name: ''
            }
        ]  
    }, action){
    console.log(`action : ${JSON.stringify(action)}`)
    switch (action.type)
    {
        case 'LIST_ROOMS':
            return ({
                selected: (state.selected === '') ? action.rooms[0].name : state.selected,
                room: action.rooms  
            })
        default:
            return state
    }
}