import React from 'react'
import {
    TextInput,
    Text,
    Platform
} from 'react-native'
import { connect } from 'react-redux'
import { pushMessage } from '../actions'

import style from '../style/Chat'

class MessageInput extends React.PureComponent {
    constructor(props, socket) {
        super(props)
        this.dispatch = props.dispatch
        this.state = {
            message: ''
        }
        this.socket = props.socket
        this.rooms = props.rooms
    }

    componentDidUpdate(PrevProps, prevState) {
        this.props.rooms = PrevProps.rooms
        this.props.callbackGetSelectedRoom(this.props.rooms.selected)
    }

    setMessage(message) {
        this.setState({message})
    }

    render() {
        return (
            <TextInput returnKeyType='send' style={(Platform.OS === 'ios') ? style.MessageInputIOS : style.MessageInputAndroid} 
                onChangeText={(text) => this.setMessage(text)}
                placeholder='Type a message...'
                value={this.state.message}
                onSubmitEditing={(text) => {
                    this.props.dispatchPushMessage(this.state.message, this.socket,
                        this.props.rooms.selected)
                    this.setMessage('')
                }}
            />
        )
    }
}

const mapStateToProps = state => ({
    rooms: state.rooms
})

const mapDispatchToProps = dispatch => ({
    dispatchPushMessage: (text, socket, room) => dispatch(pushMessage(text, socket, room)),
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MessageInput)
