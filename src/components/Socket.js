import React from 'react'
import { connect } from 'react-redux'

class Socket extends React.Component {
    constructor(props) {
        super(props)
        console.log(props)
        this.socket = props.socket
        this.rooms = props.rooms
        this.dispatch = props.dispatch
        this.socket.on('connection', (data) => {
            this.props.dispatchRooms(data)
        })
    }
    componentDidUpdate(PrevProps, prevState) {
        this.props.rooms = PrevProps.rooms
        this.socket.on(this.props.rooms.selected, (data) => {
            console.log(`${data}`)
            this.props.dispatchMessages(data)
        })
    }

    render() {
        return (null)
    }
}

export default connect()(Socket)
