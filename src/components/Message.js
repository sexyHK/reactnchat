import React from 'react'
import {
    Text,
    Image,
    View
} from 'react-native'

import PropTypes from 'prop-types'
import style from '../style/Chat'

class Message extends React.PureComponent {
    constructor(props) {
        super(props)
        this.title = props.title
    }
    render() {
        return (
            <View>
                <Image
                    source={{uri: `data:image/${this.props.avatar.ext};base64,${this.props.avatar.pic}`}}
                    style={style.avatar}
                />
                <Text style={style.MessageBubble}>
                    {this.title}
                </Text>
            </View>
        )
    }
}

Message.propTypes = {
    title: PropTypes.string.isRequired
}

export default Message