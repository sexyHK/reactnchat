import React from 'react'
import { Image } from 'react-native'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import style from '../style/Chat'

class Avatar extends React.PureComponent {
    
    constructor(props) {
        super(props)
        this.avatar = props.avatar
    }
    componentDidUpdate(PrevProps, prevState) {
        this.props.avatar = PrevProps.avatar
        this.props.callbackSetAvatar({
            uri: this.props.avatar.uri,
            name: this.props.avatar.name,
            ext: this.props.avatar.ext,
            pic: this.props.avatar.pic
        })
    }

    render() {
        return (
            <Image
                source={{uri: `data:image/jpeg;base64,${this.props.avatar.pic}`}}
                style={style.avatar}
            />
        )
    }
}

Avatar.propTypes = {
    avatar: PropTypes.shape({
        uri: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        ext: PropTypes.string.isRequired,
        pic: PropTypes.string.isRequired
    }).isRequired
}

export default Avatar