import React from 'react'
import {
    View,
    ScrollView
} from 'react-native'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import style from '../style/Chat'
import Message from './Message'
import Expo from 'expo'

class MessageView extends React.PureComponent {
    
    constructor(props) {
        super(props)
        this.messages = props.messages
        this.rooms = props.rooms
        this.profile = props.profile
        this.avatar = props.avatar
    }

    componentDidUpdate(PrevProps, prevState) {
        this.props.messages = PrevProps.messages
        this.props.rooms = PrevProps.rooms
        this.props.profile = PrevProps.profile
    }

    render() {
        return (
            <View style={style.MessageBubbleContainer}>
                <ScrollView>
                {this.props.messages.map(msg =>
                    <Message avatar={this.props.avatar} key={msg.id}
                    title={msg.message} />
                )}
               </ScrollView>
            </View>
        )
    }
}

MessageView.propTypes = {
    messages: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.number.isRequired,
            message: PropTypes.string.isRequired
        }).isRequired
    ).isRequired,
    rooms: PropTypes.shape({
        selected: PropTypes.string.isRequired,
        rooms: PropTypes.arrayOf(
            PropTypes.shape({
                id: PropTypes.number.isRequired,
                name: PropTypes.string.isRequired
            })
        )
    }).isRequired
}

export default MessageView