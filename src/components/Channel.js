import React from 'react'
import {
  KeyboardAvoidingView,
  Text,
  View,
  Button,
  TextInput,
  StatusBar
} from 'react-native';
import style from '../style/Chat'
import LoaderMessage from '../containers/LoaderMessage'
import MessageInput from './MessageInput'
import Socket from './Socket'
import { getMessages, getListRooms } from '../actions';
import SocketIOClient from 'socket.io-client'
import { connect } from 'react-redux'
import { config } from '../config.json'

class Channel extends React.PureComponent {
    static navigationOptions = {
        header: null
    };

    constructor(props) {
        super(props)
        this.dispatch = props.dispatch
        this.state = {
            room: ''
        }
        this.socket = SocketIOClient(config.url_chat)
        this.socket.emit('connection', '')
        this.socket.on('connection', (data) => {
            this.dispatch(getListRooms(data))
        })
    }

    componentDidUpdate(PrevProps, prevState) {
        this.socket.on(this.state.room, (data) => {
            console.log(`data : ${data}`)
            this.dispatch(getMessages(data))
        })
    }

    callbackGetSelectedRoom = (room) => {
        this.setState({ room: room})
    }

    render() {
        return (
            <KeyboardAvoidingView style={style.Channel} behavior="padding" enabled>
                <StatusBar hidden={true} />
                <LoaderMessage />
                <MessageInput callbackGetSelectedRoom={this.callbackGetSelectedRoom} 
                    socket={this.socket} />
            </KeyboardAvoidingView>
        );
    }
}

export default connect()(Channel)
