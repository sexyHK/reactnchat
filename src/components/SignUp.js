import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  StatusBar,
  Image,
} from 'react-native';
import { Button } from 'react-native-elements';
import style from '../style/Chat'
import { signup } from '../containers/ChatApi'
import { setAvatar, pushUserProfile } from '../actions'
import Expo from 'expo'
import LoadAvatar from '../containers/LoadAvatar'
import { connect } from 'react-redux'

class SignUp extends React.PureComponent {
  static navigationOptions = {
    header: null
  };
  constructor(props) {
    super(props)
    this.state = {
      email: '',
      pseudo: '',
      passwd: '',
      passwd2: '',
      avatar: {
        uri: '',
        name: '',
        ext: '',
        pic: ''
      }
    }
    this.dispatch = props.dispatch
  }

  setEmail(email) {
      this.setState({email})
  }
  setPseudo(pseudo) {
      this.setState({pseudo})
  }
  setPasswd(passwd) {
      this.setState({passwd})
  }
  setPasswd2(passwd2) {
      this.setState({passwd2})
  }

  callbackSetAvatar = (avatar) => {
    this.setState({ avatar: JSON.stringify(avatar)})
  }

  render() {
    return (
      <View style={style.Home}>
        <StatusBar hidden={true} />
        <Image
          source={require('../../assets/chat_logo.png')}
          style={style.logo}
        />
        <TextInput returnKeyType='next' style={style.text}
          underlineColorAndroid='transparent'
          onChangeText={(text) => this.setEmail(text)}
          placeholder='Email address'
          value={this.state.email}
        />
        <TextInput returnKeyType='next' style={style.text}
          underlineColorAndroid='transparent'
          onChangeText={(text) => this.setPseudo(text)}
          placeholder='Pseudo'
          value={this.state.pseudo}
        />
        <TextInput returnKeyType='next' style={style.text}
          underlineColorAndroid='transparent'
          onChangeText={(text) => this.setPasswd(text)}
          placeholder='Password'
          value={this.state.passwd}
          secureTextEntry={true}
        />
        <TextInput returnKeyType='next' style={style.text}
          underlineColorAndroid='transparent'
          onChangeText={(text) => this.setPasswd2(text)}
          placeholder='Confirm your password'
          value={this.state.passwd2}
          secureTextEntry={true}
        />
        <Button
          onPress={() => {
            Expo.ImagePicker.launchImageLibraryAsync({
              base64: true
            }).then(res => {
              const image = res.uri.substr(res.uri.lastIndexOf('/') + 1, res.uri.length - res.uri.lastIndexOf('/'))
              const name = image.substr(0, image.lastIndexOf('.'))
              const ext = image.substr(image.lastIndexOf('.') + 1, image.length - image.lastIndexOf('.'))
              this.callbackSetAvatar({
                uri: res.uri,
                name: name,
                ext: ext,
                pic: res.base64
              })
              this.dispatch(setAvatar(res.uri, name, ext, res.base64))
            }).catch(err => {
              console.error(err)
            })
          }}
          buttonStyle={style.btn_avatar}
          title="Import Avatar"
        />
        <LoadAvatar callbackSetAvatar={this.callbackSetAvatar}/>
        <TouchableOpacity style={style.btn_signup}
          onPress={async () => {
            console.log(this.state)
            signup(this.state)
            .then((res) => {
                if (res.status == "OK") {
                    this.dispatch(pushUserProfile(this.state))
                    this.props.navigation.navigate('Home')
                }
            }).catch(err => {
                console.error(err)
            })
        }}
        >
          <Text>Join Us !</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
export default connect()(SignUp)