import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  StatusBar,
  Image
} from 'react-native';
import style from '../style/Chat'
import { signin } from '../containers/ChatApi'
import { setAvatar } from '../actions'
import { connect } from 'react-redux'

class Home extends React.PureComponent {
  static navigationOptions = {
    header: null
  };
  constructor(props) {
    super(props)
    this.dispatch = props.dispatch
    this.state = {
      login: '',
      password: '',
      pseudo: '',
      avatar: ''
    }
  }

  setLogin(login) {
    this.setState({login})
  }
  setPassword(password) {
    this.setState({password})
  }
  setPseudo(pseudo) {
    this.setState({pseudo})
  }
  setAvatar(avatar) {
    this.setState({avatar})
  }

  render() {
    return (
      <View style={style.Home}>
        <StatusBar hidden={true} />
        <Image
          source={require('../../assets/chat_logo.png')}
          style={style.logo}
        />
        <TextInput returnKeyType='next' style={style.text}
          underlineColorAndroid='transparent'
          onChangeText={(text) => this.setLogin(text)}
          placeholder='Email or Pseudo'
          value={this.state.login}
        />
        <TextInput returnKeyType='next' style={style.text}
          underlineColorAndroid='transparent'
          onChangeText={(text) => this.setPassword(text)}
          placeholder='Password'
          value={this.state.password}
          secureTextEntry={true}
        />
        <TouchableOpacity style={style.btn_signin}
          onPress={async () => {
            signin(this.state.login, this.state.password)
            .then((res) => {
              if (res.status == "OK") {
                const pseudo = res.user.pseudo
                const avatar = JSON.parse(res.user.avatar)
                this.setPseudo(pseudo)
                this.setAvatar(avatar)
                this.dispatch(setAvatar(
                  avatar.uri,
                  avatar.name,
                  avatar.ext,
                  avatar.pic)
                )
                this.props.navigation.navigate('Channel', {
                  user: this.state
                })
              }
            }).catch(err => {
              console.error(err)
            })
          }}
        >
          <Text>Sign In</Text>
        </TouchableOpacity>
        <TouchableOpacity style={style.btn_signup}
          onPress={() => this.props.navigation.navigate('SignUp')}
        >
          <Text>Sign Up</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export default connect()(Home)