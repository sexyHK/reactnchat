import React from 'react'
import { StyleSheet } from 'react-native'

const style = StyleSheet.create({
    avatar: {
        height: 24,
        width: 24,
        borderRadius: 50,
        marginTop: 10,
        marginRight: 10,
        marginBottom: 10,
        alignSelf: 'flex-end',
        flex: 1
    },
    logo: {
        height: 96,
        width: 96,
        marginBottom: 80
    },
    text: {
      color: 'gray',
      height: 40,
      borderColor: '#fafafa',
      borderWidth: 1,
      width: '80%',
      padding: 10,
      textAlign: 'center',
      marginBottom: 20
    },
    btn_signin: {
        marginTop: 40,
        backgroundColor: '#00E676',
        padding: 10,
        width: '60%',
        alignItems: 'center'
    },
    btn_signup: {
        marginTop: 20,
        backgroundColor: '#0091EA',
        padding: 10,
        width: '60%',
        alignItems: 'center'
    },
    btn_avatar: {
        backgroundColor: "#0091EA",
        width: 300,
        borderColor: "transparent",
        borderWidth: 0,
        borderRadius: 5
    },
    Home: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#1d2025',
        alignItems: 'center',
        justifyContent: 'center',
    },
    Channel: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#1d2025',
        justifyContent: 'space-between',
    },
    MessageInputAndroid: {
        width: '100%',
        position: 'absolute',
        left: 0,
        bottom: 0,
        padding: 10,
        backgroundColor: '#fafafa',
    },
    MessageInputIOS: {
        width: '100%',
        padding: 10,
        backgroundColor: '#fafafa',
    },
    MessageBubbleContainer: {
        flex: 0.93,
        flexDirection: 'row',
    },
    MessageBubble: {
        padding: 5,
        marginTop: -40,
        backgroundColor: '#00E676',
        color: '#fafafa',
        marginRight: 40,
        marginLeft: 10,
        borderRadius: 5,
        alignSelf: 'flex-end'
    }
})

export default style