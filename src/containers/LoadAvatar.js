import { connect } from 'react-redux'
import { setAvatar } from '../actions'
import Avatar from '../components/Avatar'

const mapStateToProps = state => ({
    avatar: state.avatar
})

const mapDispatchToProps = dispatch => ({
    dispatchAvatar: (uri, name, ext, pic) => dispatch(setAvatar(uri, name, ext, pic))
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Avatar)