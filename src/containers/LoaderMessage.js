import { connect } from 'react-redux'
import { getMessages, getListRooms, getUserProfile } from '../actions'
import MessageView from '../components/MessageView'

const mapStateToProps = state => ({
    messages: state.chat,
    rooms: state.rooms,
    profil: state.profile,
    avatar: state.avatar
})

const mapDispatchToProps = dispatch => ({
    dispatchMessages: (text) => dispatch(getMessages(text)),
    dispatchRooms: (data) => dispatch(getListRooms(data)),
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MessageView)
