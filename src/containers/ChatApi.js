import FormData from 'form-data'
import { authenticate } from '../actions'
import { config } from '../config.json'

export const signin = async (login, passwd) => {
    try {
        login = 'Gh0sT'
        passwd = 'root'
        let res = await fetch(`${url_api}/signin`, {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                email: login,
                passwd: passwd
            })
        })
        let json = await res.json()
        return (json)
    } catch (error) {
        console.error(error)
    }
}

export const signup = async (form) => {
    try {
        let res = await fetch(`${config.url_api}/signup/${form.pseudo}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                profil: {
                    'email': form.email,
                    'pseudo': form.pseudo,
                    'passwd': form.passwd,
                    'passwd2': form.passwd2
                },
                avatar: form.avatar
            })
            
        })
        let json = await res.json()
        console.log(json.token)
        return (true)
    } catch (error) {
        console.error(error)
    }
}
