import React from 'react'
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import { createStackNavigator } from 'react-navigation'
import rootReducer from './src/reducers'
import HomeScreen from './src/components/Home'
import ChannelScreen from './src/components/Channel'
import SignUpScreen from './src/components/SignUp'

const store = createStore(rootReducer)

const RootStack = createStackNavigator(
  {
    Home: HomeScreen,
    Channel: ChannelScreen,
    SignUp: SignUpScreen
  },
  {
    initialRouteName: 'Home',
  }
);

export default class App extends React.PureComponent {
  render() {
    return (
      <Provider store={store}>
        <RootStack />
      </Provider>
    );
  }
}